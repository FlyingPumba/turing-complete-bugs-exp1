import sys
from numpy import random
from numpy import array
from c_helper import C_Tester
from ch_helper import Ch_Tester

# Sorting parameters
tests_number = 20
test_size = 10
test_range = 30

# Error logs
verbose = False
logging = True

def generate_test_case():
	test = random.randint(test_range, size=test_size)
	sorted = 1*test
	sorted.sort()
	return test.tolist(), sorted.tolist()

if __name__ == "__main__":
	# generate tests
	print 'Generating tests'
	tests = []
	expected = []
	for i in range(tests_number):
		test, sorted = generate_test_case()
		tests.append(test)
		expected.append(sorted)

	# check default C program works as expected
	print 'Validating default C program'
	c_tester = C_Tester(tests, expected)
	c_tester.set_logging(True)
	c_tester.set_verbose(False)
	ok = c_tester.validate_original()
	if ok:
		print 'Default C program passed tests'
	else:
		sys.exit(1)

	# check default Charity program works as expected
	print 'Validating default Charity program'
	ch_tester = Ch_Tester(tests, expected)
	ch_tester.set_logging(True)
	ch_tester.set_verbose(False)
	ok = ch_tester.validate_original()
	if ok:
		print 'Default Ch program passed tests'
	else:
		sys.exit(1)

	if len(sys.argv) > 2 and sys.argv[1] == "time":
		tests_number = int(sys.argv[2])
		print 'Generating ' + `tests_number` + ' tests'
		tests = []
		expected = []
		for i in range(tests_number):
			test, sorted = generate_test_case()
			tests.append(test)
			expected.append(sorted)
		print 'Measuring C program\'s time'
		c_tester = C_Tester(tests, expected)
		c_tester.set_logging(False)
		c_tester.set_verbose(False)
		c_tester.measure_time()
		print 'Measuring Charity program\'s time'
		ch_tester = C_Tester(tests, expected)
		ch_tester.set_logging(False)
		ch_tester.set_verbose(False)
		ch_tester.measure_time()
	elif len(sys.argv) > 1 and sys.argv[1] == "regex":
		print 'Testing C mutants'
		c_tester.test_regex_method()
		print 'Testing Charity mutants'
		ch_tester.test_regex_method()
	elif len(sys.argv) > 1 and sys.argv[1] == "char":
		print 'Testing C mutants'
		c_tester.set_base_program("sort_def_minified.c")
		c_tester.test_char_method()
		print 'Testing Charity mutants'
		ch_tester.set_base_program("sort_def_minified.ch")
		ch_tester.test_char_method()
	else:
		print ''
		print "To test regex mutants: " + sys.argv[0] + " regex"
		print "To test char mutants: " + sys.argv[0] + " char"
