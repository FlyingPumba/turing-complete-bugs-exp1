import sys, os, time
import subprocess, threading

class Timeout(Exception):
    pass

class Command(object):
    def __init__(self, cmd):
        self.cmd = cmd
        self.process = None
        self.stdout = None
        self.stderr = None

    def run(self, timeout=3, input=None):
        self.process = subprocess.Popen(self.cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
        # self.process = subprocess.Popen(self.cmd, shell=True, stdin=subprocess.PIPE)
        if input:
            self.process.stdin.write(input)
        try:
            self.wait_timeout(self.process, timeout)
            self.stdout, self.stderr = self.process.communicate()
        except:
            self.process.kill()
            self.stdout = ""
            self.stderr = "ERROR: Timeout"
            self.process.returncode = -9
        finally:
            return self.stdout, self.stderr, self.process.returncode

    def wait_timeout(self, proc, timeout):
        poll_seconds = .250
        deadline = time.time()+timeout
        while time.time() < deadline and proc.poll() == None:
            time.sleep(poll_seconds)

        if proc.poll() == None:
            if float(sys.version[:3]) >= 2.6:
                proc.terminate()
            raise Timeout()
        # start = time.time()
        # end = start + seconds
        # interval = min(seconds / 1000.0, .25)
        #
        # while True:
        #     result = proc.poll()
        #     if result is not None:
        #         return result
        #     if time.time() >= end:
        #         raise Timeout()
        #     time.sleep(interval)

if __name__ == "__main__":
    run_command = Command("./aux.o")
    out, err, ret_code = run_command.run(timeout=0.3, input="8 8 7 6 5 4 3 2 1")
    print ret_code
    print "FIN"
