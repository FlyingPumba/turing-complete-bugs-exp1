import shutil
import subprocess
from command import TimerTask

log_separator = '*'*50 + '\n'

def write_to_file(file, string) :
	output_file = open(file, "w")
	output_file.write(string)
	output_file.close()

def write_log(log_file, log) :
	output_file = open(log_file, "w")
	for i in xrange(0,len(log)) :
		output_file.write(str(i) + " " + log_separator + log[i] + '\n')
	output_file.close()

def generate_recap(log_file, recap_file = None):
	if not recap_file:
		recap_file = (log_file.split('.txt'))[0] + '_recap.txt'

	recap_cmd = TimerTask("sort " + log_file + " | uniq -c | sort -nr > " + recap_file)
	recap_cmd.run()
