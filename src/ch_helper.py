import sys
import shutil
import subprocess
from command import TimerTask
import regex_mutator
import char_mutator
from logs_helper import *

# Charity
ch_sort_default_src = "sort_def.ch"
ch_sort_src = "sort.ch"

ch_interpreter_error_log = []
ch_interpreter_error_log_file = "logs/ch_interpreter_log.txt"
ch_interpreter_error_recap_file = "logs/ch_interpreter_log_recap.txt"

ch_incorrect_output_log = []
ch_incorrect_output_log_file = "logs/ch_incorrect_output_log.txt"
ch_incorrect_output_recap_file = "logs/ch_incorrect_output_log_recap.txt"

ch_mutants_log = []
ch_mutants_log_file = "logs/ch_mutants_log.txt"
#ch_mutants_recap_file = "ch_mutants_log_recap.txt"

class Ch_Tester(object):
	def __init__(self, tests, expected, base_program = ch_sort_default_src):
		self.base_program = base_program
		self.tests = tests
		self.expected = expected
		self.logging = True
		self.verbose = False

	def set_base_program(self, base_program):
		self.base_program = base_program

	def set_logging(self, logging):
		self.logging = logging

	def set_verbose(self, verbose):
		self.verbose = verbose

	def measure_time(self):
		ch_file_aux = self.base_program+'.aux'

		totalSecs = float(0)
		for i in range(len(self.tests)):
			shutil.copy(self.base_program, ch_file_aux)
			self.build_ch(ch_file_aux, self.tests[i])
			result = self.time_ch(ch_file_aux, self.tests[i])
			totalSecs += float(result)

		totalSecs = str(totalSecs)

		timeParts = str(totalSecs).split('.')
		totalSecs = int(timeParts[0])
		remaining = timeParts[1]

		totalSecs, sec = divmod(totalSecs, 60)
		hr, min = divmod(totalSecs, 60)
		print "Testing %d instances took: %d:%02d:%02d.%s" % (len(self.tests), hr, min, sec, remaining)
		return

	def validate_original(self):
		ch_file_aux = self.base_program+'.aux'
		self.time_sum = 0
		for i in range(len(self.tests)):
			shutil.copy(self.base_program, ch_file_aux)
			self.build_ch(ch_file_aux, self.tests[i])
			result = self.test_ch(ch_file_aux, self.tests[i], self.expected[i])
			if not result:
				print 'Default Charity program failed tests'
				return False
		return True

	def test_regex_method(self):
		if self.logging:
			clean_cmd = TimerTask('rm -f logs/ch_*.txt')
			clean_cmd.run()

		mutants = regex_mutator.get_all_mutants(self.base_program)
		self.test_method(mutants)

		if self.logging:
			mv_cmd = TimerTask('mkdir -p logs/regex; mv logs/ch_*.txt logs/regex')
			mv_cmd.run()

	def test_char_method(self):
		if self.logging:
			clean_cmd = TimerTask('rm -f logs/ch_*.txt')
			clean_cmd.run()

		mutants = char_mutator.get_all_mutants(self.base_program)
		self.test_method(mutants)

		if self.logging:
			mv_cmd = TimerTask('mkdir -p logs/char; mv logs/ch_*.txt logs/char')
			mv_cmd.run()

	def test_method(self, mutants):
		ch_file_aux = self.base_program+'.aux'
		ch_equivalent_mutants = 0
		ch_equivalent_mutants_numbers = []
		for i in xrange(len(mutants)):
			if not self.verbose:
				print 'Mutant number: %d/'%i,
				print '%d\r'%len(mutants),
			else:
				print 'Mutant number: %d/'%i,
				print '%d\n'%len(mutants)
			passed_all = True
			for j in range(len(self.tests)):
				write_to_file(ch_file_aux, mutants[i])
				self.build_ch(ch_file_aux, self.tests[j])
				result = self.test_ch(ch_file_aux, self.tests[j], self.expected[j])
				if not result:
					passed_all = False
					break
			if passed_all:
				ch_equivalent_mutants += 1
				ch_equivalent_mutants_numbers.append(i)

		print 'Charity results: ' + `ch_equivalent_mutants` + ' of ' + `len(mutants)` + ' passed all tests'
		print 'Charity equivalent mutant numbers are: ' + str(ch_equivalent_mutants_numbers)
		if self.logging:
			# Mark equivalent mutants
			for i in ch_equivalent_mutants_numbers:
				mutants[i] = "E" + mutants[i]

		if self.logging:
			# write Ch logs to disk
			write_log(ch_interpreter_error_log_file, ch_interpreter_error_log)
			write_log(ch_incorrect_output_log_file, ch_incorrect_output_log)
			write_log(ch_mutants_log_file, mutants)

			# Generate Ch error recaps
			generate_recap(ch_interpreter_error_log_file, ch_interpreter_error_recap_file)
			generate_recap(ch_incorrect_output_log_file, ch_incorrect_output_recap_file)

	def build_ch(self, ch_file, test):
		# write input into charity program
		# i.e. for [2 1] we should write: merge_sort{lt}( cons(succ(succ(zero)), cons(succ(zero), nil)) ).
		aux = '\nmerge_sort{lt}(' + self.generate_charity_input(test) + ').'
		with open(ch_file, "a") as myfile:
		    myfile.write(aux)

	def test_ch(self, ch_file, test, expected):
		if self.verbose: print 'Testing with: ' + str(test)

		charity_cmd = TimerTask('yes | charity-language/src/v1/charity ' + ch_file, timeout=2)
		proc = charity_cmd.run(stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		out, err = proc.communicate()
		proc.wait()

		if proc.returncode == -9:
			err += "ERROR: Timeout"
			if self.logging: ch_interpreter_error_log.append(err)
			return False
		elif proc.returncode != 0:
			if self.logging: ch_interpreter_error_log.append(err)
			return False

		# find ERRORs in output
		err = ""
		aux = out.split('\n')
		errors = 0
		for i in range(len(aux)):
			if aux[i].count("ERROR") >= 1:
				err += " ".join(aux[i].split())
				err += "\n"
				errors += 1
		if errors >= 2:
			# if there are 2 ERRORS or more we say it failed, since the working code has 1 ERROR line already (sort_def.ch)
			if self.verbose:
				print 'Charity Interpreter ERROR'
				# print err
			if self.logging: ch_interpreter_error_log.append(err)
			return False
		else:
			res = self.parse_output_from_charity(out, expected)
			if res == False:
				other_cmd = TimerTask('cat ' + ch_file + ' >> incorrect_mutants.log; echo \'\n\' >> incorrect_mutants.log')
				other_cmd.run()
			return res

	def time_ch(self, ch_file, test):
		if self.verbose: print 'Testing with: ' + str(test)

		charity_cmd = 'yes | charity-language/src/v1/charity ' + ch_file
		cmd = 'bash -c \"TIMEFORMAT=\'%9E\'; time (' + charity_cmd + '; echo \'\')\"'

		time_cmd = TimerTask(cmd, timeout=2)
		proc = time_cmd.run(stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		out, err = proc.communicate()
		proc.wait()

		print time_cmd

		return err

	#  ---------------------- AUXILIAR FUNCTIONS -------------------------------
	def generate_charity_number(self, n):
		# i.e. for 2 we should generate: succ(succ(zero))
		if n == 0:
			return 'zero'
		else:
			return 'succ(' + self.generate_charity_number(n-1) + ')'

	def generate_charity_input(self, test):
		# i.e. for [2 1] we should generate: cons(succ(succ(zero)), cons(succ(zero), nil))
		if len(test) == 0:
			return 'nil'
		else:
			return 'cons(' + self.generate_charity_number(test[0]) + ', ' + self.generate_charity_input(test[1:len(test)]) + ')'

	def number_from_charity_output(self, output):
		# output: 'succ(zero)'
		if output.startswith('succ('):
			return 1 + self.number_from_charity_output(output[5:-1])
		elif output == "zero":
			# output: 'zero'
			return 0
		else:
			raise ValueError('Output is neither succ nor zero, is: ' + output)

	def list_from_charity_output(self, output):
		# output: '[zero, succ(zero)]'
		# remove brackets
		if len(output) <= 2 or output[0] != '[' or output[len(output)-1] != ']':
			#raise ValueError('Output not correctly formated [...], it\'s: ' + output)
			return []

		output = output[1:-1] # output: zero, succ(zero)
		# split elements
		output = output.split(',')
		lst = []
		# for each element, transform it into a number
		for s in output:
			# convert multiple spaces into one
			aux = " ".join(s.split())
			lst.append(self.number_from_charity_output(aux))
		return lst

	def parse_output_from_charity(self, output, expected):
		# in order to get the output from a valid Charity output,
		# we need to locate and process a line like:
		# 'Charity>> [zero, succ(zero)] : list(nat)'

		# print 'PARSING'
		# split output into lines
		aux = output.splitlines()
		real_output = ""
		ch_start = "Charity>> "
		ch_end = " : list(nat)"
		found = False
		# reverse iterate until we find a mathcing line
		for s in list(reversed(aux)):
			if s.startswith(ch_start) and s.endswith(ch_end):
				# save line and strip start and end
				# real_output: '[zero, succ(zero)]'
				real_output = s[len(ch_start):-len(ch_end)]
				found = True
				break

		if not found:
			#raise ValueError('Output not correctly formated, it\'s: ' + output)
			if self.logging:
				ch_incorrect_output_log.append('output: \"' + str(output) + '\"\nexpected: \"' + str(expected_list) + '\"\n')
			return False

		# Clean remaining 'Charity>> ' from output
		while real_output.startswith(ch_start):
			real_output = real_output[len(ch_start):]

		ch_list = self.list_from_charity_output(real_output)
		expected_list = list(expected)
		if expected_list == ch_list:
			return True
		else:
			if self.logging:
				ch_incorrect_output_log.append('output: \"' + str(ch_list) + '\"\nexpected: \"' + str(expected_list) + '\"\n')
			return False

if __name__ == "__main__":
	ch_tester = Ch_Tester([[6,8,3,4,5,6,1]], [[1,3,4,5,6,6,8]])
	ch_tester.set_logging(True)
	ch_tester.set_verbose(True)
	ok = ch_tester.validate_original()
	if not ok:
		sys.exit(1)
	ch_tester.test_regex_method()
