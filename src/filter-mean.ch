def getMean : list(int) -> int = l   => div_int(ADD(l), LENGHT(l)).

 def ADD: list int -> int
   = ln       => {| nil : () => 0
                  | cons: pn => add_int pn
                  |}
                     ln.

 def LENGHT: list A -> int
           = l      => {| nil : ()     => 0
                        | cons: (_, n) => add_int(n ,1)
                        |}
                           l.

def filter{p: A * A -> bool}: list A * A -> list A
   = (l, f) => {| nil : ()           => []
           | cons: (a, r) | p(a,f) => cons (a, r)
                          | ..  => r
           |}
              l.

def filterByMean : list int -> list int = l => filter{lt_int}(l, getMean(l)).
