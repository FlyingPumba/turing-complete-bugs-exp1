import os
import re
import sys
import random

### Mutation tricks ###

NULL_STRING = " "

mutation_trick = {
	" < " : [ " != ", " > ", " <= ", " >= ", " == " ],
	" > " : [ " != ", " < ", " <= ", " >= ", " == " ],
	"<=" : [ " != ", " < ", " > ", " >= ",  "==" ],
	">=" : [ " != ", " < ", " <= ", " > ",  "==" ],
	"==" : [ " != ", " = ", " < ",  " > ", " <= ", " >= " ],
	"==" : [ " != ", " = ", " < ",  " > ", " <= ", " >= " ],
	"!=" : [ " == ", " = ", " < ",  " > ", " <= ", " >= " ],
	" = " : [ " == ", " != ", " < ",  " > ", " <= ", " >= ", " = 0 * ", " = 0 ;//", " = NULL; //", " = ! " ],

	" + " : [ " - ", " * ", " / ", " % " ],
	" - " : [ " + ", " * ", " / ", " % " ],
	" * " : [ " + ", " - ", " / ", " % " ],

	" / " : [ " % ", " * ", " + ", " - " ],
 	" % " : [ " / ", " + ", " - ", " * " ],

	" + 1" : [ " - 1", "+ 0", "+ 2", "- 2" ],
	" - 1" : [ " + 1", "+ 0", "+ 2", "- 2" ],

	" & " : [ " | ", " ^ " ],
	" | " : [ " & ", " ^ " ],
	" ^ " : [ " & ", " | " ],

	" ~" : [ " !", NULL_STRING ],
	" !" : [ " ~", NULL_STRING ],

	" && " : [ " & ", " || "," && !" ],

	" || " : [ " | ", " && ", " || !" ],

	" >> " : [ " << " ],
	" << " : [ " >> " ],

	" << 1" : [ " << 0"," << -1", "<< 2" ],
	" >> 1" : [ " >> 0", " >> -1", ">> 2" ],

	"++" : [ "--" ],
	"--" : [ "++" ],

	"++;" : [ "--;", "+=2;", "-=2;" ],
	"++)" : [ "--)", "+=2)", "-=2)" ],
	"--;" : [ "++;", "+=2;", "-=2;" ],
	"--)" : [ "++)", "+=2)", "-=2)" ],

	" true "  :  " false ",
	" false " :  " true  ",

	"if (" : [ "if ( ! ", "if ( ~ ", "if ( true || ", "if ( false && " ],
	"while (" : [ "while ( ! ", "while ( ~ ", "while ( false && " ],
	"for (i = 0" : [ "for (i = 1", "for (i = 2" ],

	"break;" : [ "{;}" ],
	"continue;" : [ "{;}" ],

	"return " : [ "return 0; //", "return 1; //", "return NULL; //", "return -1; //", "return 2* ", "return -1 * " ],

	"[ " : [ "[ -1 + ", "[ 1 + ", "[ 0 * "  ],

	"(": [ " (! " ],

	");": [ "*0);", "*-1);", "*2);" ],
	"," : [ ", ! ", ", 0 * ", ", -1 * ", ", 2 *" ],
	" ? " : [ " && false ? ", " || true ? " ],
	" int " : [ " short int ", " char "  ],
	" signed " : [ " unsigned " ],
	" unsigned " : [ " signed " ],
	" long " : [ " int ", " short int ", " char " ],
	" float ": [ " int " ],
	" double ": [ " int " ],
}

def get_all_mutants(input_file):
	mutants = []
	source_code = open(input_file).read().split('\n')
	number_of_lines_of_code = len(source_code)

	# explore all lines
	for line in xrange(number_of_lines_of_code):
		# do not mutate preprocessor, assert statements or empty lines
		line_strip = source_code[line].strip()
		if line_strip.startswith("#") or line_strip.startswith("assert") or line_strip.startswith("rf") or not line_strip:
			continue

		for op in mutation_trick.keys():
			# search for substrings we can mutate
			# number_of_substrings_found = source_code[line].count(op)
			substrings =  find_all(op, source_code[line])

			# process each substring found
			for index in substrings:
				for trick in mutation_trick[op]:
					mutant = list(source_code)
					mutant[line] = mutant[line][0:index] + mutant[line][index:].replace(op, trick, 1)
					mutants.append(mutant)
					# sys.stderr.write("Original Line  : "+line_strip+"\n")
					# sys.stderr.write("\n==> @ Line: "+mutant[line]+"\n\n")

	# return each mutant as one string
	for i in xrange(len(mutants)):
		aux = ""
		for j in xrange(len(mutants[i])):
			aux += mutants[i][j] + '\n'
		mutants[i] = aux

	return mutants

def write_to_file (mutant_file_name, source_code, mutated_line_number, mutated_line):
	output_log = ""
	output_file = open(mutant_file_name, "w")
	for i in xrange(0,len(source_code)) :
		if i == mutated_line_number :
			#output_file.write("/* XXX: original code was : "+source_code[i]+" */\n")
			output_log += "/* XXX: original code was : "+source_code[i]+" */\n"
			output_log += mutated_line+"\n"
			output_file.write(mutated_line+"\n")
		else :
			output_file.write(source_code[i]+"\n")
			output_log += source_code[i]+"\n";

	output_file.close()
	return output_log

def find_all(pattern, text):
	index = 0
	indexs = []
	while index < len(text):
		index = text.find(pattern, index)
		if index == -1:
			break
		indexs.append(index)
		index += len(pattern)
	return indexs

if __name__ == "__main__":
	get_all_mutants("sort_def.c")
