## Experiment: Are non Turing complete programming languages less prone to bugs ?

Current programming languages being compared:

* Non-Turing complete languages: [Charity](https://en.wikipedia.org/wiki/Charity_%28programming_language%29)
* Turing complete languages: [C](https://en.wikipedia.org/wiki/C_%28programming_language%29)

To run the experiment:

1. Compile Charity:

* In 32 bits: ```cd src/charity-language/src/v1; ./configure; make```
* In 64 bits: ```cd src/charity-language/src/v1; ./configure32; make```

2. Run script: ```cd ../../../; python test.py```

Parameters of experiment are hardcoded (and can be changed) inside ```test.py```: ```mutants```, ```test_size``` and ```test_range```.
