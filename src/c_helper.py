import sys
import shutil
import subprocess
from command import TimerTask
import regex_mutator
import char_mutator
from logs_helper import *

# Error logs
c_sort_default_src = "sort_def.c"
c_sort_src = "sort.c"
c_sort_object = "sort.o"

c_compiler_error_log = []
c_compiler_error_log_file = "logs/c_compiler_log.txt"
c_compiler_error_recap_file = "logs/c_compiler_log_recap.txt"

c_runtime_error_log = []
c_runtime_error_log_file = "logs/c_runtime_log.txt"
c_runtime_error_recap_file = "logs/c_runtime_log_recap.txt"

c_incorrect_output_log = []
c_incorrect_output_log_file = "logs/c_incorrect_output_log.txt"
c_incorrect_output_recap_file = "logs/c_incorrect_output_log_recap.txt"
MAX_OUTPUT = 2000

c_mutants_log_file = "logs/c_mutants_log.txt"
#c_mutants_recap_file = "c_mutants_log_recap.txt"

class C_Tester(object):
	def __init__(self, tests, expected, base_program = c_sort_default_src):
		self.base_program = base_program
		self.tests = tests
		self.expected = expected
		self.logging = True
		self.verbose = False

	def set_base_program(self, base_program):
		self.base_program = base_program

	def set_logging(self, logging):
		self.logging = logging

	def set_verbose(self, verbose):
		self.verbose = verbose

	def measure_time(self):
		shutil.copy(self.base_program, c_sort_src)
		err, ret_code = self.build_c()
		if ret_code:
			print 'Default C program failed build'
			if self.verbose: print err
			return
		totalSecs = float(0)
		for i in range(len(self.tests)):
			result = self.time_c(self.tests[i])
			totalSecs += float(result)

		totalSecs = str(totalSecs)

		timeParts = str(totalSecs).split('.')
		totalSecs = int(timeParts[0])
		remaining = timeParts[1]

		totalSecs, sec = divmod(totalSecs, 60)
		hr, min = divmod(totalSecs, 60)
		print "Testing %d instances took: %d:%02d:%02d.%s" % (len(self.tests), hr, min, sec, remaining)
		return

	def validate_original(self):
		shutil.copy(self.base_program, c_sort_src)
		err, ret_code = self.build_c()
		if ret_code:
			print 'Default C program failed build'
			if self.verbose: print err
			return False
		for i in range(len(self.tests)):
			result = self.test_c(self.tests[i], self.expected[i])
			if not result:
				print 'Default C program failed tests'
				return False
		return True

	def test_regex_method(self):
		if self.logging:
			clean_cmd = TimerTask('rm -f logs/c_*.txt')
			clean_cmd.run()

		mutants = regex_mutator.get_all_mutants(self.base_program)
		self.test_method(mutants)

		if self.logging:
			mv_cmd = TimerTask('mkdir -p logs/regex; mv logs/c_*.txt logs/regex')
			mv_cmd.run()

	def test_char_method(self):
		if self.logging:
			clean_cmd = TimerTask('rm -f logs/c_*.txt')
			clean_cmd.run()

		mutants = char_mutator.get_all_mutants(self.base_program)
		self.test_method(mutants)

		if self.logging:
			mv_cmd = TimerTask('mkdir -p logs/char; mv logs/c_*.txt logs/char')
			mv_cmd.run()

	def test_method(self, mutants):
		c_equivalent_mutants = 0
		c_equivalent_mutants_numbers = []
		for i in xrange(len(mutants)):
			if not self.verbose:
				print 'Mutant number: %d/'%i,
				print '%d\r'%len(mutants),
			else:
				print 'Mutant number: %d/'%i,
				print '%d\n'%len(mutants)
			err, ret_code = self.build_c(mutants[i])
			if ret_code:
				# build failed
				if self.verbose:
					print 'C mutant failed build'
					print err
				if self.logging: c_compiler_error_log.append(err)
				continue
			passed_all = True
			for j in range(len(self.tests)):
				result = self.test_c(self.tests[j], self.expected[j])
				if not result:
					passed_all = False
					break
			if passed_all:
				c_equivalent_mutants += 1
				c_equivalent_mutants_numbers.append(i)

		print 'C results: ' + `c_equivalent_mutants` + ' of ' + `len(mutants)` + ' passed all tests'
		print 'C equivalent mutant numbers are: ' + str(c_equivalent_mutants_numbers)
		if self.logging:
			# Mark equivalent mutants
			for i in c_equivalent_mutants_numbers:
				mutants[i] = "E" + mutants[i]

		if self.logging:
			# write C logs to disk
			write_log(c_compiler_error_log_file, c_compiler_error_log)
			write_log(c_runtime_error_log_file, c_runtime_error_log)
			write_log(c_incorrect_output_log_file, c_incorrect_output_log)
			write_log(c_mutants_log_file, mutants)

			# Generate C error recaps
			generate_recap(c_compiler_error_log_file, c_compiler_error_recap_file)
			generate_recap(c_runtime_error_log_file, c_runtime_error_recap_file)
			generate_recap(c_incorrect_output_log_file, c_incorrect_output_recap_file)

	def build_c(self, mutant=None):
		if mutant:
			write_to_file(c_sort_src, mutant)
		# try to compile file
		build_cmd = TimerTask("gcc " + c_sort_src + " -o " + c_sort_object, timeout = 2)
		proc = build_cmd.run(stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		out, err = proc.communicate()
		proc.wait()
		return err, proc.returncode

	def test_c(self, test, expected):
		# get string from test and expected
		# we are gonna remove the brackets in the string ('[',']'), and
		# convert each multiple space into one.
		str_test = " ".join(str(test)[1:-1].split(', '))
		str_expected = " ".join(str(expected)[1:-1].split(', '))
		if self.verbose: print 'Testing with: ' + str(test)

		run_cmd = TimerTask("./"+c_sort_object, timeout=1)
		proc = run_cmd.run(stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		out, err = proc.communicate(input=`len(test)` + " " + str_test)
		proc.wait()

		if proc.returncode == -9:
			err = "ERROR: Timeout"

		if proc.returncode != 0:
			if self.logging:
				c_runtime_error_log.append(err)
				return False
		else:
			return self.parse_output_from_c(out, str_expected)

	def time_c(self, test):
		str_test = " ".join(str(test)[1:-1].split(', '))
		if self.verbose: print 'Testing with: ' + str(test)

		cmd = 'bash -c \"TIMEFORMAT=\'%9E\'; time (./'+c_sort_object + '; echo \'\')\"'

		run_cmd = TimerTask(cmd, timeout=1)
		proc = run_cmd.run(stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
		out, err = proc.communicate(input=`len(test)` + " " + str_test)
		proc.wait()

		return err

	#  ---------------------- AUXILIAR FUNCTIONS -------------------------------
	def parse_output_from_c(self, output, expected):
		if output == expected:
			# print 'RIGHT ANSWER'
			return True
		else:
			if self.verbose:
				print 'WRONG ANSWER'
				if len(output) <= MAX_OUTPUT:
					print 'output is: \"' + output + '\"'
				else:
					print 'output is: \"' + output[:MAX_OUTPUT/2] + '[...]' + output[len(output) - MAX_OUTPUT/2:len(output)] + '\"'
				print 'but expected was: \"' + expected + '\"'
			if self.logging:
				if len(output) <= MAX_OUTPUT:
					c_incorrect_output_log.append('output: \"' + output + '\"\nexpected: \"' + expected + '\"\n')
				else:
					c_incorrect_output_log.append('output: \"' + output[:MAX_OUTPUT/2] + '[...]' + output[len(output) - MAX_OUTPUT/2:len(output)] + '\"\nexpected: \"' + expected + '\"\n')
			return False

if __name__ == "__main__":
	c_tester = C_Tester([[6,8,3,4,5,6,1]], [[1,3,4,5,6,6,8]])
	c_tester.set_logging(True)
	c_tester.set_verbose(True)
	ok = c_tester.validate_original()
	if not ok:
		sys.exit(1)
	c_tester.test_regex_method()
