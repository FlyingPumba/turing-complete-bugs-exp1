rf "ch-libs/PRELUDE.ch".
rf "ch-libs/colist.ch".
rf "ch-libs/cobTree.ch".

def split: list(A) -> list(A) * list(A)
    = l => {| nil : ()            => ([], [])
            | cons: (a, (l1, l2)) => (l2, cons (a, l1))
            |}
               l.

def build_m_tree: list(A) -> cobTree(list(A), 1)
    = l => (| []  => debTree: b0 []
            | [a] => debTree: b0 [a]
            | l   => debTree: b1 ((), split l)
            |)
               l.

def merge_sort{lt_A: A * A -> bool}: list(A) -> list(A)
    = l => { len  => colist_2_list ( fold_cobTree{ x      => x
                                                 , (_, p) => merge{lt_A} p
                                                 }
                                                   ( cobTree{ list_2_colist
                                                            , () => ()
                                                            }
                                                              build_m_tree l
                                                   , ( len
                                                     , (delist: ff)
                                                     )
                                                   )
                                   ,
                                     len
                                   )
           }
             length l.
