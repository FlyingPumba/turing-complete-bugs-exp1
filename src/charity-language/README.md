_charity-language_
==================

Source code of two interpreters for the Charity language:

1. The original [C implementation](src/v1), by the [Charity Development Group](http://pll.cpsc.ucalgary.ca/charity1/www/home.html), dated 28 September 2000

2. An [SML implementation](src/v2), by Min Zeng, dated March 2003

Includes an [archival version of the Charity website](doc/README.md), all available [literature](doc/pdf), [example programs](doc/ch), and [binary releases](bin) of the original implementation.


#### Additional work

* Gillian Posey’s [Circuits package](more/circuits), dated 13 March 1997

* Robin Cockett’s work on [model checking](doc/model-checking.md), including:
    - [HML checker](more/hml), dated 26 October 2000
    - [Modal Mu checker](more/mm), dated 22 November 2000
    - [Circuit checker](more/wires), dated 2 September 2001


### Literature

* 1991, R. Cockett, [“Introduction to distributive categories”](doc/pdf/cockett-1991.pdf)
* 1991, T. Fukushima, [“Charity user manual”](doc/pdf/fukushima-1991.pdf)
* 1992, R. Cockett, T. Fukushima, [“About Charity”](doc/pdf/cockett-1992a.pdf)
* 1992, R. Cockett, [“Distributive logic”](doc/pdf/cockett-1992b.pdf)
* 1992, R. Cockett, D. Spencer, [“Strong categorical datatypes I”](doc/pdf/cockett-1992c.pdf)
* 1992, R. Cockett, D. Spencer, [“Strong categorical datatypes II: A term logic for categorical programming”](doc/pdf/cockett-1992d.pdf)
* 1992, M. Hermann, “A lazy graph reduction machine for Charity: Charity abstract reduction machine (CHARM)”
* 1992, T. Simpson, R. Cockett, [“Sequentializing programs defined by pattern matching”](doc/pdf/simpson-1992.pdf)
* 1993, R. Cockett, [“Examples of Charity term logic proofs”](doc/pdf/cockett-1993.pdf)
* 1993, M. Schroeder, [“Charity grammar”](doc/pdf/schroeder-1993a.pdf)
* 1993, M. Schroeder, [“CHIRP: A front end for Charity”](doc/pdf/schroeder-1993b.pdf)
* 1993, D. Spencer, [“Categorical programming with functorial strength”](doc/pdf/spencer-1993.pdf)
* 1993, B. Yee, [“The CHARM project: A back end to the Charity interpreter”](doc/pdf/yee-1993.pdf)
* 1994, C. Tuckey, [“The implementation of pattern matching in Charity”](doc/pdf/tuckey-1994.pdf)
* 1995, B. Yee, [“Implementing the Charity abstract machine”](doc/pdf/yee-1995.pdf)
* 1996, R. Cockett, [“Charitable thoughts”](doc/pdf/cockett-1996.pdf)
* 1996, T. Fukushima, C. Tuckey, [“Charity user manual”](doc/pdf/fukushima-1996.pdf) (final version)
* 1996, P. Vesely, [“Categorical combinators for Charity”](doc/pdf/vesely-1996.pdf)
* 1997, M. Schroeder, [“Higher-order Charity”](doc/pdf/schroeder-1997.pdf)
* 1997, C. Tuckey, [“Pattern matching in Charity”](doc/pdf/tuckey-1997.pdf)
* 1997, P. Vesely, [“Typechecking the Charity term logic”](doc/pdf/vesely-1997.pdf)
* 2003, M. Zeng, [“An implementation of Charity”](doc/pdf/zeng-2003.pdf)


#### Additional literature

* 1991, R. Blute, R. Cockett, R. Seely, T. Trimble, [“Natural deduction and coherence for weakly distributive categories”](doc/pdf/more/blute-1991.pdf)
* 1994, R. Cockett, D. Spooner, [“SProc categorically”](doc/pdf/more/cockett-1994.pdf)
* 1995, R. Cockett, D. Spooner, [“Categories for synchrony and asynchrony”](doc/pdf/more/cockett-1995.pdf)
* 1996, R. Blute, R. Cockett, R. Seely, T. Trimble, [“Natural deduction and coherence for weakly distributive categories”](doc/pdf/more/blute-1996.pdf) (final version)
* 1996, R. Cockett, D. Spooner, [“Constructing process categories”](doc/pdf/more/cockett-1996.pdf)
* 1997, R. Cockett, D. Spooner, [“Constructing process categories”](doc/pdf/more/cockett-1997a.pdf) (final version)
* 1997, R. Cockett, R. Seely, [“Proof theory for full intuitionistic linear logic, bilinear logic, and MIX categories”](doc/pdf/more/cockett-1997b.pdf)
* 1997, D. Spooner, [“Building process categories”](doc/pdf/more/spooner-1997.pdf)


### Related work

* [_Total_ functional programming](https://github.com/mietek/total-functional-programming)
* [_Totally_ functional programming](https://github.com/mietek/totally-functional-programming)
* [The ET language](https://github.com/mietek/et-language)


About
-----

Packaged by [Miëtek Bak](https://mietek.io/).
