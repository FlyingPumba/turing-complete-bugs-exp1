#include <stdio.h>
#include <stdlib.h>

void merge (int *a, int n, int m) {
    int i, j, k;
    int *x = malloc(n * sizeof (int));
    for (i = 0, j = m, k = 0; k < n; k++) {
        x[k] = j == n      ? a[i++]
             : i == m      ? a[j++]
             : a[j] < a[i] ? a[j++]
             :               a[i++];
    }
    for (i = 0; i < n; i++) {
        a[i] = x[i];
    }
    free(x);
}

void merge_sort (int *a, int n) {
    if (n < 2)
        return;
    int m = n / 2;
    merge_sort(a, m);
    merge_sort(a + m, n - m);
    merge(a, n, m);
}

int main () {
    int n;
    scanf("%d", &n);
    int array[n];
    int i;
    for (i = 0; i < n; ++i) {
        scanf("%d", &array[i]);
    }
    merge_sort(array, n);
    for (i = 0; i < n-1; ++i) {
        printf("%d ", array[i]);
    }
    printf("%d", array[n-1]);
    return 0;
}
