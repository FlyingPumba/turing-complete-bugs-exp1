\section{Experiment}\label{description}

\subsection{Languages}\label{desc-lang}
The experiment uses two different programming languages, which will introduce now:
\begin{itemize}
    \item \textbf{C}: is a general-purpose, imperative computer programming language,
     supporting structured programming, lexical variable scope and recursion, while
      a static type system prevents many unintended operations.

      C was originally developed by Dennis Ritchie between 1969 and 1973 at AT\&T Bell Labs.

    \item \textbf{Charity}: is an experimental purely functional non-Turing complete programming
        language, developed at the University of Calgary, Canada, by The Charity Development Group
        under the supervision of Robin Cockett.

        Charity is based on the theory of strong categorical data types.
        These are divided into two subclasses: the inductive data types (built up by constructors in the familiar way)
        and the coinductive data types (broken down by destructors).
        Programs over these data types are expressed by folds (catamorphisms) and by unfolds (anamorphisms), respectively.

        Catamorphisms represent inductive recursion where each recursive step
        consumes at least one constructor. Since each steps takes only finite time,
         together this ensures that when consuming a (finite) data structure,
         the whole recursion always terminates.
        Anamorphisms represent co-inductive recursion where each recursive step
         is guarded by a constructor. This means that although the result can
         be infinite, each part (a constructor) of the constructed data structure
          is produced in finite time.


        We list some, but not all, of Charity's features below:
        \begin{itemize}
            \item Charity is``pure", and supports lazy evaluation.
            \item Charity is higher-order.
            \item All Charity computations terminate (up to user input).
        \end{itemize}

        % The language allows ordinary recursive data types, such as might be found in ML,
        %  which are required to be finite, and corecursive data types, which are allowed
        %  to be potentially infinite. The control structure for operating on recursive
        %  data types is primitive recursion or paramorphism, and the control structure
        %  for corecursive data types is primitive co-recursion or apomorphism.
        %  Neither control structure can operate over the other kind of data, so
        %  all paramorphisms terminate and all apomorphisms are productive.
\end{itemize}

\subsection{Procedure}\label{desc-proc}
For each language, an implementation of Mergesort is used. [Reference to Appendix with code]

The experiment consists on:
\begin{enumerate}
    \item The \textit{test cases} are generated: 20 integer arrays of size 10
        with each integer randomly chosen in range [1,30].
    \item C and Charity implementations of Mergesort are validated against generated tests to check that correct output is returned.
    \item C and Charity mutants are obtained by different methods.
    \item Each mutant is tested with the same arrays to check if it's equivalent to the original implementation.
\end{enumerate}

In the following subsections we'll explain two different methods to obtain mutants.

\subsection{Regex method}\label{desc-regex}
We define a set of mutation operators [Reference to Appendix with all operators] consisting of:
\begin{itemize}
    \item Matching part: a string that will be matched against the original source code.
    \item Replacing part: a string that will replace the matching part in the mutated code.
\end{itemize}

Then we exhaustively explore all possible matching parts appearing in the original
source code and replace them with all possible replacements. This procedure can
be seen in the following algorithm:

\begin{figure}[H]
\begin{mdframed}[backgroundcolor=black!8, linewidth=0pt]
\begin{Verbatim}[commandchars=\\\{\}]
Init mutants = empty array
Init source_code = base program split by end lines

# explore all lines
For line in source_code:
    # do not mutate preprocessor, assert statements or empty lines
    If line starts with '#', 'assert', 'rf' or is empty:
        continue with next line
    For operator in mutation_operators:
        # Search for substrings we can mutate
        Init substrings = array of substrings in the line
            that match the operator
        For s in substrings:
            For replacement in operator:
                # create new mutant
                Init new_mutant = source_code
                Put new_mutant[line] = source_code[line] with s replaced
                Add new_mutant to mutants

# return each mutant as one string
For mutant in mutants:
    Join lines of mutant in one string
return mutants
\end{Verbatim}
\end{mdframed}
\caption{Algorithm of Regex method to generate mutants}\label{algorithm-regex}
\end{figure}

\subsection{Character method}\label{desc-char}
We define a set of printable chars:
\begin{center}
\{ !\#\$\%\&'()*+,-.\/:;<=>?@\{$|$\}\textasciitilde[\textbackslash]\^\_`0123456789abcdefghijklmnopqrstuvwxyz \}
\end{center}

Then for each character in the original source code we replace it with each character,
different from the original one, in the printable chars set. This procedure can
be seen in the following algorithm:

\begin{figure}[H]
\begin{mdframed}[backgroundcolor=black!8, linewidth=0pt]
\begin{Verbatim}[commandchars=\\\{\}]
Init mutants = empty array
Init source_code = base program split by end lines

# explore all lines
For line in source_code:
    # do not mutate preprocessor, assert statements or empty lines
    If line starts with '#', 'assert', 'rf' or is empty:
        continue with next line
    # explore all chars in line
    For char in line:
        # do not change whitespaces
        If char is whitespace:
            continue with next char
        For replacement in printable_chars:
            If char == replacement:
                continue with next replacement
            # create new mutant
            Init new_mutant = source_code
            Put new_mutant[line] = source_code[line] with char replaced
            Add new_mutant to mutants

# return each mutant as one string
For mutant in mutants:
    Join lines of mutant in one string
return mutants
\end{Verbatim}
\end{mdframed}
\caption{Algorithm of Character method to generate mutants}\label{algorithm-char}
\end{figure}

\textit{Note:} to reduce the amount of mutants on this method, we used a \text{minified}
version of the source code used for the Regex method. In this \text{minified} version
all variables and functions, where possible, were reduced to one-character long.
